﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using Utils;

namespace App
{
    internal static class Program
    {
        [ExcludeFromCodeCoverage]
        private static void Main(string[] args)
        {
            CultureInfo formatProvider = new CultureInfo("en-US");
            var operand = args[1];
            var firstNum = int.Parse(args[0], formatProvider);
            var secondNum = int.Parse(args[2], formatProvider);

            var calc = new Calculator(firstNum, secondNum);

            switch (operand)
            {
                case "add":
                    Console.WriteLine(firstNum + " " + operand + " " + secondNum + " = " + calc.Add());
                    break;
                case "sub":
                    Console.WriteLine(firstNum + " " + operand + " " + secondNum + " = " + calc.Sub());
                    break;
                case "mul":
                    Console.WriteLine(firstNum + " " + operand + " " + secondNum + " = " + calc.Mul());
                    break;
                case "div":
                    Console.WriteLine(firstNum + " " + operand + " " + secondNum + " = " + calc.Div());
                    break;
                default:
                    Console.WriteLine("Wrong input!");
                    break;
            }
        }
    }
}