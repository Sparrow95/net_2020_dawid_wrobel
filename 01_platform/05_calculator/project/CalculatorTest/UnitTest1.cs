using Utils;
using Xunit;


namespace CalculatorTest
{
    public class UnitTests1
    {
        [Fact]
        public void ShouldCreateInstanceOfCalculatorClass()
        {
            Calculator calc = new Calculator(2, 5);
            Assert.NotNull(calc);
        }

        [Fact]
        public void ShouldReturnSumOfTwoGivenNumbers()
        {
            Calculator calc = new Calculator(2, 5);
            Assert.Equal(7, calc.Add());
        }

        [Fact]
        public void ShouldReturnDifferenceOfTwoGivenNumbers()
        {
            Calculator calc = new Calculator(2, 5);
            Assert.Equal(-3, calc.Sub());
        }

        [Fact]
        public void ShouldReturnProductOfTwoGivenNumbers()
        {
            Calculator calc = new Calculator(3, 6);
            Assert.Equal(18, calc.Mul());
        }

        [Fact]
        public void ShouldReturnQuotientOfTwoGivenNumbers()
        {
            Calculator calc = new Calculator(10, 2);
            Assert.Equal(5, calc.Div());
        }
    }
}