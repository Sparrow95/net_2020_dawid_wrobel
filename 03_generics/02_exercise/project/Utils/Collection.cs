﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Utils
{
    public class Collection<T> : IEnumerable<T>
    {
        private T[] _elements;
        public int Factor { get; }
        public int Capacity { get; private set; }
        public int Count { get; private set; }

        public Collection(int capacity = 1, int factor = 2)
        {
            if (capacity < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(capacity));
            }

            if (factor < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(factor));
            }

            _elements = new T[capacity];
            Capacity = capacity;
            Factor = factor;
            Count = 0;
        }

        public void Add(T item)
        {
            if (Count == Capacity)
            {
                Array.Resize(ref _elements, Capacity * Factor);
                Capacity *= Factor;
            }

            _elements[Count++] = item;
        }

        public T this[int i] => _elements[i];

        public IEnumerator<T> GetEnumerator()
        {
            return new CollectionEnum<T>(_elements);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    public sealed class CollectionEnum<T> : IEnumerator<T>
    {
        private readonly T[] _elements;

        private int _position = -1;
        public T Current
        {
            get
            {
                try
                {
                    return _elements[_position];
                }
                catch (IndexOutOfRangeException)
                {
                    throw new InvalidOperationException();
                }
            }
        }

        public CollectionEnum(T[] elem)
        {
            _elements = elem;
        }

        public bool MoveNext()
        {
            _position++;
            return _position < _elements.Length;
        }

        public void Reset()
        {
            _position = -1;
        }

        object? IEnumerator.Current => Current;

        public void Dispose()
        {
        }
    }
}