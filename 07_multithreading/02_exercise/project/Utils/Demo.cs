﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Utils
{
    public class Demo
    {
        private readonly int[] _data;

        public Demo(int[] data)
        {
            _data = data;
        }

        public int Sum()
        {
            var sum = 0;
            for (int i = 0; i < _data.Length; i++)
            {
                sum += _data[i];
            }

            return sum;
        }

        public int SumForeach()
        {
            var sum = 0;
            foreach (var item in _data)
            {
                sum += item;
            }

            return sum;
        }

        public int SumLinq()
        {
            return _data.Sum();
        }

        private delegate void ThreadAction(int start, int stop);
        
       private void RunStandaloneThreads(int count, ThreadAction action)
        {
            var listOfThreads = new List<Thread>();
            for (var i = 0; i < count; i++)
            {
                var lengthOfSegment = _data.Length / count + 1;
                var tmp = i;
                
                var thread = (tmp + 1) * lengthOfSegment > _data.Length
                    ? new Thread(() => action(tmp * lengthOfSegment, _data.Length))
                    : new Thread(() => action(tmp * lengthOfSegment, (tmp + 1) * lengthOfSegment));
 
                thread.Start();
                listOfThreads.Add(thread);
            }

            foreach (var thread in listOfThreads)
                thread.Join();
        }

        public int SumThreadsInterlocked(int count)
        {
            var sum = 0;
            RunStandaloneThreads(count, (start, stop) =>
            {
                for (int i = start; i < stop; i++)
                {
                    Interlocked.Add(ref sum, _data[i]);
                }
            });
            return sum;
        }


        public int SumThreads(int count)
        {
            var sum = 0;
            RunStandaloneThreads(count, (start, stop) =>
            {
                var partialSum = 0;
                for (int i = start; i < stop; i++)
                {
                    partialSum += _data[i];
                }

                Interlocked.Add(ref sum, partialSum);
            });
            return sum;
        }

        private void RunPoolThreads(int count, ThreadAction action)
        {
            var events = new List<ManualResetEvent>();
            for (int i = 0; i < count; i++)
            {
                var lengthOfSegment = _data.Length / count + 1;
                var tmp = i;
                var resetEvent = new ManualResetEvent(false);
                if ((tmp + 1) * lengthOfSegment > _data.Length)
                {
                    ThreadPool.QueueUserWorkItem(_ =>
                    {
                        action(
                            (tmp * lengthOfSegment), _data.Length);
                        resetEvent.Set();
                    });
                }
                else
                {
                    ThreadPool.QueueUserWorkItem(_ =>
                    {
                        action(tmp * lengthOfSegment, (tmp + 1) * lengthOfSegment);
                        resetEvent.Set();
                    });
                }
                events.Add(resetEvent);
            }
 
            WaitHandle.WaitAll(events.ToArray<WaitHandle>());
        }

        public int SumPoolThreads(int count)
        {
            var sum = 0;
            RunPoolThreads(count, (start, stop) =>
            {
                var partialSum = 0;
                for (int i = start; i < stop; i++)
                {
                    partialSum += _data[i];
                }

                Interlocked.Add(ref sum, partialSum);
            });
            return sum;
        }

        public int SumTpl()
        {
            var sum = 0;
            Parallel.For(0, _data.Length, i => { Interlocked.Add(ref sum, _data[i]); });
            return sum;
        }
    }
}