using System;
using System.IO;
using Moq;
using Utils;
using Xunit;

namespace Test
{
    [Collection("Seq")]
    public class WindowTest
    {
        [Fact]
        public void WindowCanBeCreated()
        {
            var window = new Window();
            
            Assert.NotNull(window);
        }

        [Fact]
        public void WindowHandlesClicksOnItsButtons()
        {
            var mockTextWriter = new Mock<TextWriter>(MockBehavior.Loose);
            var savedDefaultOutput = Console.Out;
            Console.SetOut(mockTextWriter.Object);
            mockTextWriter.Setup(tw =>
                tw.WriteLine("Tu handler OkButton w Window!")).Verifiable();
            mockTextWriter.Setup(tw =>
                tw.WriteLine("Tu handler CancelButton w Window!")).Verifiable();
            
            var window = new Window();
            Assert.False(window.HandledButtonOkClick);
            Assert.False(window.HandledButtonCancelClick);
            window.SimulateClicks();
            Assert.True(window.HandledButtonCancelClick);
            Assert.True(window.HandledButtonOkClick);
            
            Console.SetOut(savedDefaultOutput);
            mockTextWriter.Verify();
        }
    }
}