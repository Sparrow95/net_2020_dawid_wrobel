using System;
using Moq;
using Utils;
using Xunit;

namespace Test
{
    [Collection("Seq")]
    public class ButtonTest
    {
        [Fact]
        public void ButtonCanBeCreated()
        {
            var button = new Button();
            Assert.NotNull(button);
        }

        [Fact]
        public void ButtonCanHaveLabelSet()
        {
            const string? label = "ButtonLabelTest";
            var button = new Button(label);
            Assert.Equal(label, button.Label);
        }

        [Fact]
        public void ButtonReactsProperlyWhenClicked()
        {
            var mockEventHandler = new Mock<EventHandler<ClickedEventArgs>>(MockBehavior.Loose);
            var button = new Button {Clicked = mockEventHandler.Object};

            mockEventHandler.Setup(clicked =>
                clicked.Invoke(button,
                    new ClickedEventArgs("Kliknieto przycisk klasy Button o nazwie: " + button.Label))).Verifiable();
            
            button.Click();
            
            mockEventHandler.Verify();
        }
    }
}