using System;
using System.IO;
using Moq;
using Utils;
using Xunit;

namespace Test
{
    [Collection("Seq")]
    public class LoggingButtonTest
    {
        [Fact]
        public void LoggingButtonCanBeCreated()
        {
            var logButton = new LoggingButton();
            Assert.NotNull(logButton);
        }

        [Fact]
        public void LoggingButtonReactsProperlyWhenClicked()
        {
            var mockEventHandler = new Mock<EventHandler<ClickedEventArgs>>(MockBehavior.Loose);
            var mockTextWriter = new Mock<TextWriter>(MockBehavior.Loose);
            var savedDefaultOutput = Console.Out;
            Console.SetOut(mockTextWriter.Object);
            var loggingButton = new LoggingButton {Clicked = mockEventHandler.Object};
            
            mockTextWriter.Setup(tw =>
                tw.WriteLine("Kliknieto przycisk LoggingButton!\tWysylam sygnal!")).Verifiable();
            
            mockEventHandler.Setup(clicked =>
                clicked.Invoke(loggingButton,
                    new ClickedEventArgs("Kliknieto przycisk klasy LoggingButton o nazwie: " + loggingButton.Label)))
                        .Verifiable();
            
            loggingButton.Click();
            
            Console.SetOut(savedDefaultOutput);
            
            mockEventHandler.Verify();
            mockTextWriter.Verify();
        }
    }
}