using System;

namespace Utils
{
    public class LoggingButton : Button
    {
        public LoggingButton(string label = "DefaultLoggingButtonLabel") : base(label)
        {
        }

        protected override void OnClicked()
        {
            Console.WriteLine("Kliknieto przycisk LoggingButton!\tWysylam sygnal!");
            Clicked?.Invoke(this,
                new ClickedEventArgs("Kliknieto przycisk klasy LoggingButton o nazwie: " + Label));
        }
    }
}