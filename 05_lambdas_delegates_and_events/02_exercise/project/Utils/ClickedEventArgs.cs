using System;

namespace Utils
{
    public class ClickedEventArgs : EventArgs
    {
        public readonly string Label;

        public ClickedEventArgs(string label = "DefaultEventArgsLabel")
        {
            Label = label;
        }
    }
}