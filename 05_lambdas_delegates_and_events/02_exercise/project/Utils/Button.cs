using System;

namespace Utils
{
    public class Button
    {
        public readonly string Label;
        public EventHandler<ClickedEventArgs>? Clicked;

        public Button(string label = "DefaultButtonLabel")
        {
            Label = label;
        }

        public void Click()
        {
            OnClicked();
        }

        protected virtual void OnClicked()
        {
            Clicked?.Invoke(this, new ClickedEventArgs("Kliknieto przycisk klasy Button o nazwie: " + Label));
        }
    }
}