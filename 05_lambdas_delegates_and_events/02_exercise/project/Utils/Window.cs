using System;

namespace Utils
{
    public partial class Window
    {
        private void ButtonOk_Clicked(object sender, ClickedEventArgs e)
        {
            HandledButtonOkClick = true;
            Console.WriteLine("Tu handler OkButton w Window!");
        }

        private void ButtonCancel_Clicked(object sender, ClickedEventArgs e)
        {
            HandledButtonCancelClick = true;
            Console.WriteLine("Tu handler CancelButton w Window!");
        }
    }
}