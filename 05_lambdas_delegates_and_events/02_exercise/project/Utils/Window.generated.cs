using System.Diagnostics.CodeAnalysis;

namespace Utils
{
    public partial class Window
    {
        private LoggingButton _buttonOK;
        private LoggingButton _buttonCancel;

        public bool HandledButtonOkClick { get; private set; }
        public bool HandledButtonCancelClick { get; private set; }
        
        [ExcludeFromCodeCoverage]
        public Window()
        {
            initializeComponent();
        }
        
        [ExcludeFromCodeCoverage]
        private void initializeComponent()
        {
            _buttonOK = new LoggingButton("OkLoggingButton");
            _buttonCancel = new LoggingButton("CancelLoggingButton");
            _buttonOK.Clicked += ButtonOk_Clicked;
            _buttonCancel.Clicked += ButtonCancel_Clicked;
        }

        [ExcludeFromCodeCoverage]
        public void SimulateClicks()
        {
            _buttonOK?.Click();
            _buttonCancel?.Click();
        }
    }
}