using System;

namespace Modeling
{
    public class SinSource : ISource
    {
        public double Frequency { get; }
        public double Phase { get; }
        public double Amplitude { get; }

        public SinSource(double freq, double phase, double ampl)
        {
            Frequency = freq;
            Phase = phase;
            Amplitude = ampl;
        }

        public double Sample(double time)
        {
            return Math.Sin((time * 2 * Math.PI + Phase) * Frequency) * Amplitude;
        }
    }
}