namespace Modeling
{
    public class Simulation
    {
        private readonly ISource _source;
        private readonly ISeismogram _seismogram;
        public Simulation(ISource source, ISeismogram seismogram)
        {
            _source = source;
            _seismogram = seismogram;
        }

        public void Execute(double startTime, double timeInterval, double endTime)
        {
            if (timeInterval > 0)
            {
                while (startTime <= endTime)
                {
                    var value = _source.Sample(startTime);
                    _seismogram.Store(startTime, value);
                    startTime += timeInterval;
                }
            }
            else
            {
                while (startTime >= endTime)
                {
                    var value = _source.Sample(startTime);
                    _seismogram.Store(startTime, value);
                    startTime += timeInterval;
                }
            }
        }
    }
}