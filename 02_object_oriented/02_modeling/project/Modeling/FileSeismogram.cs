using System.IO;

namespace Modeling
{
    public class FileSeismogram : ISeismogram
    {
        private readonly TextWriter _tw;

        public FileSeismogram(TextWriter writer)
        {
            _tw = writer;
        }

        public void Close()
        {
            _tw.Close();
        }

        public void Store(double time, double value)
        {
            _tw.WriteLine($"{time} {value}");
        }
    }
}