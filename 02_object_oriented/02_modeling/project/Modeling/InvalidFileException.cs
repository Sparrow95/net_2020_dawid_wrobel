using System;

namespace Modeling
{
    public class InvalidFileException : Exception
    {
        public InvalidFileException()
        {
        }

        public InvalidFileException(string? msg) :
            base(msg)
        {
        }

        public InvalidFileException(string msg, Exception exception) :
            base(msg, exception)
        {
        }
    }
}