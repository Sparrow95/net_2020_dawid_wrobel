using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace Modeling
{
    public class FileSource : ISource
    {
        private readonly Dictionary<double, double> _data;

        public FileSource(TextReader? reader)
        {
            if (reader == null)
                throw new ArgumentNullException(nameof(reader), "TextReader in FileSource ctor cannot be null!");

            _data = new Dictionary<double, double>();
            CultureInfo formatProvider = new CultureInfo("us-US");

            while (true)
            {
                var lineFromReader = reader.ReadLine();
                if (lineFromReader == null)
                {
                    break;
                }

                string[] valuesFromLine = lineFromReader.Split(' ');
                if (valuesFromLine.Length != 2)
                {
                    throw new InvalidFileException("Invalid data from FileSource!");
                }

                _data[Convert.ToDouble(valuesFromLine[0], formatProvider)] =
                    Convert.ToDouble(valuesFromLine[1], formatProvider);
            }
        }

        public double Sample(double time)
        {
            switch (_data.Count)
            {
                case 0:
                    return 0.0;
                case 1:
                    return _data.First().Value;
            }

            var iter = _data.GetEnumerator();
            iter.MoveNext();
            for (var i = 0; i < _data.Count; i++)
            {
                if (i == 0 && time <= iter.Current.Key)
                {
                    return iter.Current.Value;
                }

                if (i == _data.Count - 1 && time >= iter.Current.Key)
                {
                    return iter.Current.Value;
                }

                if (Math.Abs(iter.Current.Key - time) < 0.01)
                {
                    return iter.Current.Value;
                }

                var earlierValue = iter.Current.Value;
                var earlierTime = iter.Current.Key;
                iter.MoveNext();

                if (Math.Abs(iter.Current.Key - time) < 0.01)
                {
                    return iter.Current.Value;
                }

                if (time >= earlierTime && time <= iter.Current.Key)
                {
                    return (earlierValue + iter.Current.Value) / 2.0;
                }
            }

            iter.Dispose();
            return double.MinValue;
        }
    }
}